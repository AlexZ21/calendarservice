/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzhur.calendarservice.controllers;

import com.alexzhur.calendarservice.components.EventManager;
import com.alexzhur.calendarservice.entities.Event;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author alex
 */
@Controller
public class Router {

    @Autowired
    private EventManager eventManager;

    @RequestMapping("/")
    public String index(Model model) {
        return "index";
    }

    @RequestMapping("/test")
    public @ResponseBody
    String test(Model model) {

        Event event = new Event();

        event.setTitle("Sano");
        event.setEmail("ZSashka21@gmail.com");
        Date created = new Date();
        event.setDate(created);

        eventManager.add(event);

        return "test";
    }
    
    @RequestMapping("/add")
    public @ResponseBody
    String add(@RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "descr", required = false) String descr,
            @RequestParam(value = "email", required = false) String eamil,
            @RequestParam(value = "notify", required = false) boolean notify) {
        
        Event event = new Event();

        event.setTitle(title);
        event.setDescr(descr);
        event.setEmail(eamil);
        event.setNotify(notify);
        
        event.setDate(new Date());
        
        eventManager.add(event);

        return "success";
    }

    @RequestMapping("/edit")
    public @ResponseBody
    String edit(@RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "descr", required = false) String descr,
            @RequestParam(value = "email", required = false) String eamil,
            @RequestParam(value = "created", required = false) String created,
            @RequestParam(value = "notify", required = false) boolean notify) {

        Event event = new Event();

        event.setId(id);
        event.setTitle(title);
        event.setDescr(descr);
        event.setEmail(eamil);
        event.setNotify(notify);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date dateStr = null;

        try {
            dateStr = formatter.parse(created);
        } catch (ParseException ex) {
            Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
        }
        event.setDate(dateStr);

        eventManager.update(event);

        return "success";
    }
    
    @RequestMapping("/remove")
    public @ResponseBody
    String remove(@RequestParam(value = "id", required = false) Integer id) {
        
        Event event = new Event();
        event.setId(id);
        
        eventManager.remove(event);
        
        return "success";
    }

    @RequestMapping("/find")
    public @ResponseBody
    String find(@RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "created", required = false) String created) {

        HashMap<String, Object> fields = new HashMap<>();

        if (id != null) {
            fields.put("id", id);
        }

        if (created != null) {
            if (!created.equals("underfind")) {

                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                Date dateStr = null;

                try {
                    dateStr = formatter.parse(created);
                } catch (ParseException ex) {
                    Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
                }

                fields.put("created", dateStr);
            }
        }

        JSONArray array = new JSONArray();
        for (Event event : eventManager.find(fields)) {
            JSONObject object = new JSONObject();
            object.put("id", event.getId());
            object.put("title", event.getTitle());
            object.put("descr", event.getDescr());
            object.put("email", event.getEmail());
            object.put("created", event.getDate().toString());
            object.put("notify", event.getNotify());
            array.add(object);
        }

        return array.toJSONString();
    }

}
