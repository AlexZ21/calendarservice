/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzhur.calendarservice.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "events")
public class Event {
    
    @Id
    @GeneratedValue
    private int id;
    @Column(columnDefinition="varchar(255)")
    private String title;
    @Column(columnDefinition="text")
    private String descr;
    @Temporal(TemporalType.DATE)
    @Column(name = "created")
    private Date created;
    @Column(columnDefinition="varchar(255)")
    private String email;
    @Column(nullable = false, columnDefinition="bit", length = 1)
    private boolean notify;
    @Column(nullable = false, columnDefinition="bit", length = 1)
    private boolean removed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Date getDate() {
        return created;
    }

    public void setDate(Date created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public boolean getRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
    
    
    
}
