/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzhur.calendarservice.components;

import com.alexzhur.calendarservice.entities.Event;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author alex
 */
@Component
public class NotifyManager {
    
    @Autowired
    private EventManager eventManager;
    
    public void init() {
        
        int sec = 10;
        
        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                
                System.out.println("Check...");
                
                HashMap<String, Object> fields = new HashMap<>();
                
                fields.put("created", new Date());
                
                List<Event> events = eventManager.find(fields);
                
                for(Event event: events) {
                    
                    if(!event.getNotify()) {
                        System.out.print("Notify: "+event.getTitle());
                        System.out.println();
                        //event.setNotify(true);
                        //eventManager.update(event);
                    }
                    
                    
                }
                                
            }
            
        }, 0, 5*1000);
        
    }
    
}
