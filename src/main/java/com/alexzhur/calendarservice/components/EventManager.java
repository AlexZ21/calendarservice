/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzhur.calendarservice.components;

import com.alexzhur.calendarservice.entities.Event;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Component;

/**
 *
 * @author alex
 */
@Component
public class EventManager {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void add(Event p) {
        em.persist(p);
    }
    
    @Transactional
    public Event update(Event p) {
        return em.merge(p);
    }
    
    @Transactional
    public void remove(Event p) {
        Event event = em.find(Event.class, p.getId());
        em.remove(event);
    }

    @Transactional
    public List<Event> find(Map<String, Object> fields) {
        String queryStr = "SELECT e FROM " + Event.class.getName() + " e";

        Query q = null;

        if (fields.size() > 0) {

            queryStr += " WHERE ";

            String separator = "";
            for (Map.Entry<String, Object> entry : fields.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                
                queryStr += " " + separator + " " + key + " = :f_" + key;
                separator = "AND";
            }
            
            q = em.createQuery(queryStr, Event.class);

            for (Map.Entry<String, Object> entry : fields.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                q.setParameter("f_" + key, value);
            }
        } else {
            q = em.createQuery(queryStr, Event.class);
        }

        return q.getResultList();

    }

}
