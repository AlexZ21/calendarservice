/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var currentEventsList;
    var currentDate;

    $('#datepicker').dtpicker({
        "inline": true,
        "locale": "ru",
        "firstDayOfWeek": 1,
        "dateOnly": true,
        "onChanged": dateChanged
    });

    $(".events-list").on("click", ".row", function () {

        var eventKey = $(this).attr("event");

        $(".edit-form").attr("event", eventKey);

        var ev = currentEventsList[eventKey];

        $(".edit-form .input-title").val(ev.title);
        $(".edit-form .input-descr").val(ev.descr);
        $(".edit-form .input-email").val(ev.email);
        $(".edit-form .input-notify").val(ev.notify);

        $(".events-pages").animate({
            'left': -410,
        }, {
            duration: 300,
            queue: true
        });

        $(".toolbar .cancel").show();
        $(".toolbar .remove").show();
        $(".toolbar .accept").show();
        $(".toolbar .new").hide();

    });

    $(".toolbar").on("click", ".cancel", function () {

        $(".toolbar .remove").hide();
        $(".toolbar .cancel").hide();
        $(".toolbar .accept").hide();
        $(".toolbar .new").show();

        $(".events-pages").animate({
            'left': 0,
        }, {
            duration: 300,
            queue: true
        });

    });

    $(".toolbar").on("click", ".accept", function () {

        var title = $(".edit-form .input-title").val();
        var descr = $(".edit-form .input-descr").val();
        var email = $(".edit-form .input-email").val();
        var notify = $(".edit-form .input-notify").val();

        if ($(".edit-form").attr("event")) {
            var key = $(".edit-form").attr("event");
            var id = currentEventsList[key].id;
            var created = currentEventsList[key].created;
            $.post("/CalendarService/edit", {id: id, title: title,
                descr: descr, email: email, notify: notify, created: created}, function (data) {
                console.log(data);
                updateEventsList();
                $(".toolbar .remove").hide();
                $(".toolbar .cancel").hide();
                $(".toolbar .accept").hide();
                $(".toolbar .new").show();
            });

            $(".events-pages").animate({
                'left': 0,
            }, {
                duration: 300,
                queue: true
            });
        } else {

            $.post("/CalendarService/add", {title: title,
                descr: descr, email: email, notify: notify}, function (data) {
                console.log(data);
                updateEventsList();
                $(".toolbar .remove").hide();
                $(".toolbar .cancel").hide();
                $(".toolbar .accept").hide();
                $(".toolbar .new").show();
            });

            $(".events-pages").animate({
                'left': 0,
            }, {
                duration: 300,
                queue: true
            });

        }
    });

    $(".toolbar").on("click", ".remove", function () {
        if ($(".edit-form").attr("event")) {
            var key = $(".edit-form").attr("event");
            var id = currentEventsList[key].id;
            $.post("/CalendarService/remove", {id: id}, function (data) {
                updateEventsList();
                $(".toolbar .remove").hide();
                $(".toolbar .cancel").hide();
                $(".toolbar .accept").hide();
                $(".toolbar .new").show();
            });

            $(".events-pages").animate({
                'left': 0,
            }, {
                duration: 300,
                queue: true
            });
        }
    });

    $(".toolbar").on("click", ".new", function () {

        $(".edit-form").attr("event", "");

        $(".edit-form .input-title").val("");
        $(".edit-form .input-descr").val("");
        $(".edit-form .input-email").val("");
        $(".edit-form .input-notify").val("ev.notify");

        $(".events-pages").animate({
            'left': -410,
        }, {
            duration: 300,
            queue: true
        });

        $(".toolbar .cancel").show();
        $(".toolbar .accept").show();
        $(".toolbar .new").hide();

    });

    function dateChanged(date) {
        currentDate = date;
        updateEventsList();
        $(".toolbar .remove").hide();
        $(".toolbar .cancel").hide();
        $(".toolbar .accept").hide();
        $(".toolbar .new").show();
        $(".events-pages").animate({
            'left': 0,
        }, {
            duration: 300,
            queue: true
        });
    }

    function updateEventsList() {
        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1;
        var yyyy = currentDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var stringDate = dd + "-" + mm + "-" + yyyy;

        $.get("/CalendarService/find", {created: stringDate}, function (data) {
            console.log(data);

            $(".events-list").html("");

            currentEventsList = $.parseJSON(data);
            for (var key in currentEventsList) {
                var val = currentEventsList[key];

                if (val.descr === "null")
                    val.descr = "";

                $(".events-list").append("<div event=\"" + key + "\" class=\"row\">" +
                        "<div class=\"title\">" + val.title + "</div>" +
                        "<div class=\"descr\">" + val.descr + "</div>" +
                        "</div>");

            }


        });
    }

});




