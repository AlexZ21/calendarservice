<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="<c:url value="/resources/css/base.css" />" type="text/css"> 
        <link rel="stylesheet" href="<c:url value="/resources/css/jquery.simple-dtpicker.css" />" type="text/css"> 

        <title>JSP Page</title>
    </head>
    <body>

        <div class="wrapper">
            <div class="header">
                <div class="title">
                    Календарь событий
                </div>
            </div>
            <div class="content">
                <div class="content-wrapper">
                    <div class="left">
                        <div id="datepicker"> </div>
                    </div>
                    <div class="right">
                        <div class="events">
                            <div class="events-pages">
                                <div class="page">
                                    <div class="header">
                                        События
                                    </div> 
                                    <div class="events-list">

                                    </div>
                                </div>
                                <div class="page">
                                    <div class="header">
                                        Редактировать событие
                                    </div> 
                                    <div class="edit-form">
                                        <div class="field">
                                            <div class="title">Заголовок</div>
                                            <input type="text" class="input-title">
                                        </div>
                                        <div class="field">
                                            <div class="title">Описание</div>
                                            <textarea class="input-descr"></textarea>
                                        </div>
                                        <div class="field">
                                            <div class="title">E-mail</div>
                                            <input type="text" class="input-email">
                                        </div>  
                                        <div class="field">
                                            <div class="title">Оповещать</div>
                                            <input type="checkbox" class="input-email">
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="toolbar">
                            <div class="new"></div>
                            <div class="remove"></div>
                            <div class="accept"></div>
                            <div class="cancel"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">

            </div>
        </div>

        <script language="javascript" type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.2.min.js" />"></script>
        <script language="javascript" type="text/javascript" src="<c:url value="/resources/js/jquery.simple-dtpicker.js" />"></script>
        <script language="javascript" type="text/javascript" src="<c:url value="/resources/js/base.js" />"></script>
    </body>
</html>
